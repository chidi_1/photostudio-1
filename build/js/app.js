function get_session_data(){
    $('.popup .inp-room').val($('.calc--left .inp-interier').val());
    $('.popup .inp-look').val($('.calc--left .inp-look').val());
    $('.popup .inp-date').datepicker();
}

function get_session_date(){
    $('.popup .inp-date').val($('.service-booking .inp-date').val());
    $('.popup .inp-time').val($('.service-booking .select-time option:selected').val());
}

$(document).ready(function () {
    if ($('.range-slider').length) {
        $(".range-slider").each(function () {
            var slider = $(this);
            var input = slider.closest('.label-slider').find('input');
            var min = slider.data('min');
            var max = slider.data('max');
            var step = slider.data('step');
            var current = slider.data('current');
            var number = slider.closest('.label-slider').find('.label-slider-number');

            slider.slider({
                min: min,
                max: max,
                value: current,
                step: step,
                slide: function (event, ui) {
                    input.val(ui.value);

                    number.find('span').each(function () {
                        $(this).removeClass('active');
                        if (Number($(this).data('number')) == ui.value) {
                            $(this).addClass('active');
                        }
                    });

                    calc();
                },
                create: function (event, ui) {
                    input.val($(this).slider("value"));

                    for (var i = min; i <= max; i = i + step) {
                        var html = '<span data-number="' + i + '">' + i + '</span>';
                        number.append(html);
                    }

                    number.find('span').each(function () {
                        $(this).removeClass('active');
                        if (Number($(this).data('number')) == current) {
                            $(this).addClass('active');
                        }
                    });

                    calc();
                }
            });
        })
    }

    function calc(){
        var start = 6500;
        var interier = Number($('.inp-interier').val());
        var int_text="";
        var look = Number($('.inp-look').val());
        var look_text="";
        var total = 0;

        total = start + (interier - 1)*2000;

        if(interier < look){
            total = total + (look - interier)*1000;
        }

        switch (interier){
            case 1:
                int_text = '1 иньерьере';
                break;
            case 2:
                int_text = '2 иньерьерах';
                break;
            case 3:
                int_text = '3 иньерьерах';
                break;
            case 4:
                int_text = '4 иньерьерах';
                break;
            case 5:
                int_text = '5 иньерьерах';
                break;
        }

        switch (look){
            case 1:
                look_text = '1 образа';
                break;
            case 2:
                look_text = '2 образов';
                break;
            case 3:
                look_text = '3 образов';
                break;
            case 4:
                look_text = '4 образов';
                break;
            case 5:
                look_text = '5 образов';
                break;
        }

        $('.calc-text .calc-text-room').text(int_text);
        $('.calc-text .calc-text-look').text(look_text);

        $('.calc-text--value').text(total + 'р')
    }

    if ($('.selet-styled').length) {
        $('.selet-styled').each(function () {
            $(this).styler({
                onSelectClosed: function () {
                    if ($(this).hasClass('clothes-select')) {
                        var select = $(this);
                        var url = select.data('url');
                        var method = select.data('method');
                        var data = select.find('select option:selected').prop('value');
                        var container = $('.clothes-container');


                        $.ajax({
                            type: method,
                            url: url,
                            data: {'data': data},
                            success: function (data) {
                                console.log(data);
                                container.html(data);
                            }
                        });
                    }
                }
            });
        })
    }

    if ($('.inp-phone').length) {
        $('.inp-phone').mask('+7 (999) 999-99-99');
    };

    if ($('.inp-date').length) {
        $('.inp-date').datepicker()
    };

    if ($('.fancy').length) {
        $('.fancy').fancybox();
    };



    if ($('#map').length) {
        ymaps.ready(function () {
            var myMap,
                myPlacemark;

            myMap = new ymaps.Map('map', {
                center: [55.792575, 49.175273],
                zoom: 17,
                controls: ["zoomControl", "fullscreenControl"]
            }, {});
            myPlacemark = new ymaps.Placemark(myMap.getCenter(), {}, {});

            myMap.behaviors.disable('scrollZoom');

            myMap.geoObjects.add(myPlacemark);

        });
    }
    if ($('#map1').length) {
        ymaps.ready(function () {
            var myMap,
                myPlacemark;

            myMap = new ymaps.Map('map2', {
                center: [55.792575, 49.175273],
                zoom: 17,
                controls: ["zoomControl", "fullscreenControl"]
            }, {});
            myPlacemark = new ymaps.Placemark(myMap.getCenter(), {}, {});
            myMap.behaviors.disable('scrollZoom');

            myMap.geoObjects.add(myPlacemark);

        });
    }
    if ($('#map2').length) {
        ymaps.ready(function () {
            var myMap,
                myPlacemark;

            myMap = new ymaps.Map('map3', {
                center: [55.792575, 49.175273],
                zoom: 17,
                controls: ["zoomControl", "fullscreenControl"]
            }, {});
            myPlacemark = new ymaps.Placemark(myMap.getCenter(), {}, {});

            myMap.behaviors.disable('scrollZoom');

            myMap.geoObjects.add(myPlacemark);

        });
    }

    // маска
    if ($('.inp-phone').length) {
        $('.inp-phone').mask('+7 (999) 999-99-99');
    }
    // анимация сертификат
    if ($('.sertificate-letter-inner').length) {
        $('.sertificate-letter-inner').addClass('moved');
    }

    // табы
    $('ul.tabs--caption').each(function () {
        $(this).find('li').each(function (i) {
            $(this).click(function () {
                $(this).addClass('active').siblings().removeClass('active')
                    .closest('.tabs').find('div.tabs--content').removeClass('active').eq(i).addClass('active');
            });
        });
    });

    // слайдер работ
    if ($('.works-list').length) {
        $('.works-list').owlCarousel({
            margin: 20,
            loop: true,
            autoWidth: false,
            autoHeight: false,
            nav: true,
            dots: false,
            navText: ["Влево", "Вправо"],
            autoplay: false,
            items: 5
        });
    }

    $('.js--change-title').on('input', function () {
        $('.sertificate-form--letter h3').text($(this).val());
    });
    $('.js--change-text').on('input', function () {
        $('.sertificate-form--letter-text').text($(this).val());
    });

    $('.js--open-menu').on('click', function () {
        $('.header-bottom').addClass('open');
        return false;
    });
    $('.js--close-menu').on('click', function () {
        $('.header-bottom').removeClass('open');
        return false;
    });

    $('.js--show-full').on('click', function () {
        $('.service-calc .calc').addClass('open');
        $(this).addClass('hidden-block');
        return false;
    });

    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    $(document).on('click', '.js--form-submit', function () {
        var form = $(this).closest('.main-form');
        var errors = false;

        $(form).find('.required').each(function () {
            var val = $(this).prop('value');
            if (val == '') {
                $(this).addClass('error');
                errors = true;
            }
            else {
                if ($(this).hasClass('inp-mail')) {
                    if (validateEmail(val) == false) {
                        $(this).addClass('error');
                        errors = true;
                    }
                }
            }
        });

        if (errors == false) {
            var button_value = $(form).find('.js--form-submit').text();
            $(form).find('.js--form-submit').text('Отправляем...');

            var method = form.attr('method');
            var action = form.attr('action');
            var data = form.serialize();
            console.log(data);
            $.ajax({
                type: method,
                url: action,
                data: data,
                success: function (data) {
                    form.find('.form-input').each(function () {
                        $(this).prop('value', '')
                    });
                    $(form).find('.js--form-submit').text(button_value);
                    $(form).find('.js--sended-ok').trigger('click');
                },
                error: function (data) {
                    $(form).find('.js--form-submit').text('Ошибка');
                    setTimeout(function () {
                        $(form).find('.js--form-submit').text(button_value);
                    }, 2000);
                }
            });
        }

        return false;
    });

    $(document).on('focus', '.inp', function () {
        $(this).removeClass('error');
    });

    // анимация сервис
    if ($('.service-top--list').length) {
        var inter = 0;
        var timer = 300;
        $('.service-top--list li').each(function () {
            var block = $(this);
            setInterval(function () {
                block.addClass('animated fadeInUpSpeed');
            }, inter * timer);
            inter++;
        });
    }

    // подгрузка фото
    $('.js--load-more').on('click', function () {
        var btn = $(this);
        var url = btn.data('url');
        var method = btn.data('method');
        var container = btn.closest('section').find('.load-container');
        var data = container.find('li').length;

        $.ajax({
            type: method,
            url: url,
            data: {'data': data},
            success: function (data) {
                console.log(data)

                var parse_data = jQuery.parseJSON(data);

                if(parse_data.content == 'false'){
                    btn.addClass('hidden-block');
                }

                container.append(parse_data.html);
            }
        });

        return false;
    })

    // прокрутка страницы
    $(document).on('click', '.js--page-scroll', function() {
        $("html, body").animate({
            scrollTop: $($(this).attr("href")).offset().top
        }, {
            duration: 500
        });
        return false;
    });

    if(device.mobile() || device.tablet()){
        $('.js--show-inner').on('click', function(){
            $(this).find('.nav-inner').slideToggle(300);
            return false;
        });
        $('.js--show-inner a').on('click', function(event){
            window.location = $(this).attr('href');
            event.preventDefault();
            event.stopPropagation();
        });
    }
});
